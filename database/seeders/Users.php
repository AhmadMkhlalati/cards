<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name'=>'user',
            'email'=>'user@user.com',
            'password'=>bcrypt("123456789") ,
            'email_verified_at'=>Carbon::now()
        ]);
    }
}
