<?php

namespace Database\Seeders;

use App\Models\PaypalAccount;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class paypal extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaypalAccount::insert([
            [
                'currency'=>'USD',
                'secret'=>'EIYtfKognrz2KTmKjeNZXuvv5rIjvKP_EOWTZp7Fyijl2N5jy1oWtnvcRFcjXb2MCitNy0-azDhbA0DX',
                'client'=>'ATbtRXg2zodnhDMfxoNE-7NjOoI7Qooo-fipirskMSBM_wcvd54zu_fOAFQit_fFNHEUr7j9BVRBf0iC'
            ],
            [
                'currency'=>'EUR',
                'secret'=>'EJygZdG_ZCjYTdwBvzdZnq3zJPu517PqWzeubVfbSGYPITvlDwmSOYZb-1w0cgheDtNtcv_hf1PopLt5',
                'client'=>'AcCxn8V9ONd7WohM2JEyZEeq0hJAepTkWvw1XiSYpepgObuK8OWfza6clpVLVp2NKz10GLKLWdk2B1kq'
            ],
            [
                'currency'=>'CAD',
                'secret'=>'EIYtfKognrz2KTmKjeNZXuvv5rIjvKP_EOWTZp7Fyijl2N5jy1oWtnvcRFcjXb2MCitNy0-azDhbA0DX',
                'client'=>'ATbtRXg2zodnhDMfxoNE-7NjOoI7Qooo-fipirskMSBM_wcvd54zu_fOAFQit_fFNHEUr7j9BVRBf0iC'
            ],
        ]);
    }
}
