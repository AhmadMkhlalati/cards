<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::create([
           'name'=>'admin',
           'email'=>'admin@admin.com',
           'password'=>bcrypt("123456789") ,
            'email_verified_at'=>Carbon::now()
        ]);
    }
}
