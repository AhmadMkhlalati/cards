<?php

use App\Http\Controllers\ProfileController;
use App\Models\Card;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $cards = Card::where('user_id', Auth::id())->with('response')->get();
    $data['Paid']=0;
    $data['NotPaid']=0;

    foreach ($cards as $card){
        if($card['response']){
            $data['Paid']+=1;
        }else{
            $data['NotPaid']+=1;
        }
    }
    $data['cards']= count($cards);
    return view('dashboard',compact('data'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/card/{id}', [\App\Http\Controllers\CardsController::class, 'paypal'])->name('cards.insert')->where('id', '[0-9]+');
    Route::get('/card/create', [\App\Http\Controllers\CardsController::class, 'create'])->name('cards.create');
    Route::get('/card', [\App\Http\Controllers\CardsController::class, 'index'])->name('cards.index');
    Route::get('/card/duplicate/{id}', [\App\Http\Controllers\CardsController::class, 'duplicate'])->name('cards.duplicate');

});

Route::group(['prefix'=>'paypal'],function (){
    Route::post('/create', [\App\Http\Controllers\CardsController::class, 'paypal_create'])->name('paypal.create');
    Route::post('/capture', [\App\Http\Controllers\CardsController::class, 'paypal_capture'])->name('paypal.capture');
});

require __DIR__.'/auth.php';

require __DIR__.'/admin_auth.php';
require __DIR__.'/admin_route.php';
