<?php
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Admin\CardsController;
use \App\Http\Controllers\Admin\UserController;

Route::get('admin-cards',[CardsController::class,'index'])->name('admin.cards');
Route::get('admin-user/{id}',[UserController::class,'show'])->name('admin.show.user');
Route::get('admin-user',[UserController::class,'all'])->name('admin.all.user');
Route::get('user-verified/{id}',[UserController::class,'user_verified'])->name('admin.user.verified');
Route::get('user-un-verified/{id}',[UserController::class,'user_un_verified'])->name('admin.user.unverified');
Route::get('paying/{id}',[UserController::class,'paying'])->name('admin.user.paying');
