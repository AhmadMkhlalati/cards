<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\User;
use App\Models\CardResponse;
use App\Models\PaypalAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Services\PayPal as PayPalClient;

class CardsController extends Controller
{
    public function index(){

        return view('cards.index');
    }
    public function create()
    {
        return view('cards.create');
    }
    public function duplicate($id)
    {
        $card = Card::find($id);
        $data =   Card::create([
            'amount' => $card->amount,
            'currency' => $card->currency,
            'notes' => $card->notes,
            'status' => 0,
            'user_id' => Auth::id()
        ]);
        return redirect()->route('cards.insert',$data->id)->with( ['success' => 'Card Has been Created '] );
    }

    public function paypal($id)
    {

        $card = Card::find($id);
        $PayPal = PaypalAccount::where('currency', $card['currency'])->first();
        $ClientID = $PayPal->client;
        $Currency = $PayPal->currency;

        return view('pay', compact('ClientID', 'Currency', 'id','card'));
    }

    public function paypal_create(Request $request)
    {
        $card = Card::find($request->id);
        $PayPal = PaypalAccount::where('currency', $card['currency'])->first();
        $config = [
            'mode' => 'live',
            'live' => [
                'client_id' => $PayPal['client'],
                'client_secret' => $PayPal['secret'],
                'app_id' => 'APP-80W284485P519543T',
            ],
            'sandbox' => [
                'client_id'         => 'AdAZiHNwyKw91ucJLUXRfGjscvECxatuO5AY_ejlyIW93FCz2WjnDJryHovulvalnLW9EfhUI1CH8hTm',
                'client_secret'     => "EIoEOdQR7yUa7mqeRpmoJRE0YUmBLQoO6cZo26lLoOGyT7-iA-SGvPadALqcSnGwYpXnqOY5DdDIzwmr",
                'app_id'            => 'APP-80W284485P519543T',
            ],

            'payment_action' => 'Sale',
            'currency' => $card['currency'],
            'notify_url' => '',
            'locale' => 'en_US',
            'validate_ssl' => true,
        ];
        $provider = new PayPalClient;
        $provider->setApiCredentials($config);
        $token = $provider->getAccessToken();
        $provider->setAccessToken($token);

        $currency = $card->currency;
        $amount = $card->amount;
        $id = $request->id;

        $order = $provider->createOrder([
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "description" => "Card Use it By Id" . $id,
                "amount" => [
                    "currency_code" => $currency,
                    "value" => $amount
                ]]
            ]
        ]);
        return response()->json($order);
    }
    public function paypal_capture(Request $request){
        $card = Card::find($request->order_id);
        $PayPal = PaypalAccount::where('currency', $card['currency'])->first();
        $config = [
            'mode' => 'live',
            'live' => [
                'client_id' => $PayPal['client'],
                'client_secret' => $PayPal['secret'],
                'app_id' => 'APP-80W284485P519543T',
            ],
            'sandbox' => [
                'client_id'         => 'AdAZiHNwyKw91ucJLUXRfGjscvECxatuO5AY_ejlyIW93FCz2WjnDJryHovulvalnLW9EfhUI1CH8hTm',
                'client_secret'     => 'EIoEOdQR7yUa7mqeRpmoJRE0YUmBLQoO6cZo26lLoOGyT7-iA-SGvPadALqcSnGwYpXnqOY5DdDIzwmr',
                'app_id'            => 'APP-80W284485P519543T',
            ],

            'payment_action' => 'Sale',
            'currency' => $card['currency'],
            'notify_url' => '',
            'locale' => 'en_US',
            'validate_ssl' => true,
        ];
        $provider_payment = new PayPalClient;
        $provider_payment->setApiCredentials($config);
        $token = $provider_payment->getAccessToken();
        $provider_payment->setAccessToken($token);
        $payment_orderId = $request->payment_orderId;
        $order_id = $request->order_id;


        $result = $provider_payment->capturePaymentOrder($payment_orderId);

        $transaction =  $result['purchase_units'][0]['payments']['captures'][0];
        if ( $transaction['status'] == "COMPLETED") {
            $user = Auth::user();
            $value =$user[$card['currency']]+ $card->amount;
            User::where('id',$user->id)->update([
                $card['currency']  => $value
            ]);
            CardResponse::create([
                'card_id'=>$order_id,
                'response'=>json_encode($result),
            ]);
        }
        return $result;
    }
}
