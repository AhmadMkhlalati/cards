<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show($id){
       $user = User::find($id);
       return view('admin.user.index',compact('user'));

    }
    public function all()
    {
        return view('admin.customers.index');
    }
    public function user_verified($id)
    {
        $user = User::find($id);
        return view('admin.customers.verified',compact('user'));
    }

    public function user_un_verified($id)
    {
        $user = User::find($id);
        return view('admin.customers.unverified',compact('user'));
    }
    public function paying($id){
        $user = User::find($id);
        $user->update(['EUR'=>0,'USD'=>0,'GBP'=>0,'CAD'=>0]);
        Card::where('user_id',$user->id)->with(['response'])->whereHas('response')->where('status',0)->update(['status'=>1]);
        return redirect()->back();
    }
}
