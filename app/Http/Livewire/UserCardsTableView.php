<?php

namespace App\Http\Livewire;

use App\Models\Card;
use LaravelViews\Facades\Header;
use LaravelViews\Facades\UI;
use LaravelViews\Views\TableView;
use LaravelViews\Views\Traits\WithAlerts;


class UserCardsTableView extends TableView
{
    use WithAlerts;

    /**
     * Sets a model class to get the initial data
     */
    public $searchBy = [
        'name',
        'amount',
        'currency',
        'notes',
        'status',
        'user_id'];
    public $userId;
    protected $paginate = 10;
    public function mount($userId)
    {
        $this->userId = $userId;
    }

    protected $model = User::class;

    protected function repository()
    {
        return Card::query()->where('user_id',$this->userId)->with(['response']);
    }
    /**
     * Sets the headers of the table as you want to be displayed
     *
     * @return array<string> Array of headers
     */
    public function headers(): array
    {
        return [
            Header::title('id')->sortBy('id'),
            Header::title('name')->sortBy('name'),
            Header::title('amount')->sortBy('amount'),
            Header::title('currency')->sortBy('currency'),
            Header::title('notes')->sortBy('notes'),
            Header::title('response')->sortBy('response'),
            Header::title('status')->sortBy('status'),
        ];
    }

    /**
     * Sets the data to every cell of a single row
     *
     * @param $model Current model for each row
     */
    public function row($model): array
    {
        return [
            $model->id,
            UI::editable($model,'name'),
            UI::editable($model,'amount'),
            UI::editable($model,'currency'),
            strlen($model->notes) > 50 ? substr($model->notes,0,50)."..." : $model->notes,
            $model->response == null ?  UI::badge('UNVERIFIED', 'danger') : UI::badge('VERIFIED', 'success'),
            UI::editable($model,'status'),
        ];
    }

    public function update($model, $data)
    {
        Card::where('id',$model)->update($data);
        $this->success();
    }
}
