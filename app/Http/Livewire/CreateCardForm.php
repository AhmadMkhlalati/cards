<?php

namespace App\Http\Livewire;

use App\Models\Card;
use Illuminate\Support\Facades\Auth;
use LaravelViews\Views\Traits\WithAlerts;
use Livewire\Component;

class CreateCardForm extends Component
{
    use WithAlerts;

    public $amount;
    public $currency;
    public $notes;

    protected $rules = [
        'amount' => 'required',
        'currency' => 'required',
    ];
    public function mount()
    {
        $this->currency = 'EUR';
        $this->notes = '';
    }
    public function submit()
    {
          $this->validate($this->rules);

        // Execution doesn't reach here if validation fails.
        try {
           $data =   Card::create([
                'amount' => $this->amount,
                'currency' => $this->currency,
                'notes' => $this->notes,
                'status' => 0,
                'user_id' => Auth::id()
            ]);
            return redirect()->route('cards.insert',$data->id)->with( ['success' => 'Card Has been Created '] );
        } catch (\Exception $e) {
            return redirect()->route('cards.create')->with( ['error' => 'Card Not Created Check Your Fields'] );
        }
    }

    public function render()
    {
        return view('livewire.create-card-form');
    }
}
