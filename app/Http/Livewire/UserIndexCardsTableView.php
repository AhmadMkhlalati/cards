<?php

namespace App\Http\Livewire;

use App\Models\Card;
use Illuminate\Support\Facades\Auth;
use LaravelViews\Facades\Header;
use LaravelViews\Facades\UI;
use LaravelViews\Views\TableView;
use LaravelViews\Views\Traits\WithActions;

class UserIndexCardsTableView extends TableView
{
    use WithActions;

    /**
     * Sets a model class to get the initial data
     */
    protected $model = Card::class;

    /**
     * Sets the headers of the table as you want to be displayed
     *
     * @return array<string> Array of headers
     */
    protected $paginate = 10;
    public function headers(): array
    {
        return  [
        Header::title('id')->sortBy('id'),
        Header::title('name')->sortBy('name'),
        Header::title('amount')->sortBy('amount'),
        Header::title('currency')->sortBy('currency'),
        Header::title('notes')->sortBy('notes'),
        Header::title('status')->sortBy('status'),
    ];
    }

    protected function repository()
    {
        return Card::where('user_id',Auth::id())->with(['user','response']);
    }


    /**
     * Sets the data to every cell of a single row
     *
     * @param $model Current model for each row
     */
    public function row($model): array
    {
        return [
            $model->id,
            $model->name,
            $model->amount,
            $model->currency,
            strlen($model->notes) > 50 ? substr($model->notes,0,50)."..." : $model->notes,
            $model->response == null ? UI::badge('UNVERIFIED', 'danger'):UI::badge('VERIFIED', 'success'),
        ];
    }
}
