<?php

namespace App\Http\Livewire;

use App\Models\Card;
use LaravelViews\Facades\Header;
use LaravelViews\Facades\UI;
use LaravelViews\Views\TableView;
use LaravelViews\Views\Traits\WithAlerts;


class CardsTableView extends TableView
{
    use WithAlerts;
    /**
     * Sets a model class to get the initial data
     */
    public $paginate = 10;
    public $searchBy = [
        'amount',
        'price',
        'currency',
        'notes',
        'status',
        'user_id'];

    protected $model = Card::class;

    /**
     * Sets the headers of the table as you want to be displayed
     *
     * @return array<string> Array of headers
     */
    public function headers(): array
    {
        return [
            Header::title('id')->sortBy('id'),
            Header::title('amount')->sortBy('amount'),
            Header::title('price')->sortBy('price'),
            Header::title('currency')->sortBy('currency'),
            Header::title('notes')->sortBy('notes'),
            Header::title('status')->sortBy('status'),
            Header::title('user name'),
        ];
    }
    protected function repository()
    {
        return Card::query()->with('user');
    }

    /**
     * Sets the data to every cell of a single row
     *
     * @param $model Current model for each row
     */
    public function row($model): array
    {
        return [
           $model->id,
            UI::editable($model,'amount'),
            UI::editable($model,'price'),
            UI::editable($model,'currency'),
            strlen($model->notes) > 50 ? substr($model->notes,0,50)."..." : $model->notes,
            UI::editable($model,'status'),
            UI::link($model->user->name,route('admin.show.user',$model->user->id)),
        ];
    }

    public function update($model, $data)
    {
        Card::where('id',$model)->update($data);
        $this->success();
    }
}
