<?php

namespace App\Http\Livewire;

use LaravelViews\Facades\UI;
use LaravelViews\Views\TableView;
use App\Models\User;


class CustomersTableView extends TableView
{
    /**
     * Sets a model class to get the initial data
     */
    protected $paginate = 10;

    protected $model = User::class;
    public $searchBy = [
        'id',
        'name',
        'email',];
    /**
     * Sets the headers of the table as you want to be displayed
     *
     * @return array<string> Array of headers
     */
    public function headers(): array
    {
        return [
            'id',

            'name',
            'email',
            'verified',
            'unverified'
        ];
    }

    /**
     * Sets the data to every cell of a single row
     *
     * @param $model Current model for each row
     */
    public function row($model): array
    {
        return [
            $model->id,
            $model->name,
            $model->email,
            UI::link('verified',route('admin.user.verified',$model->id)),
            UI::link('un verified',route('admin.user.unverified',$model->id)),


        ];
    }
}
