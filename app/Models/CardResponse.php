<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardResponse extends Model
{
    use HasFactory;
    protected $fillable = [
        'card_id',
        'response',
    ];
}
