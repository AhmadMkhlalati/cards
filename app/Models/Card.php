<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'amount',
        'currency',
        'notes',
        'status',
        'user_id',
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function response(){
        return $this->hasOne(CardResponse::class,);
    }
}
