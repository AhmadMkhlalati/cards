<div class="max-w-lg mx-auto sm:px-6 lg:px-8 ">
    <div class="flex justify-center pt-8 sm:pt-0">
        <h1 class="text-3xl font-semibold">Create Cards</h1>
    </div>

    <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden  sm:rounded-lg ">
        <div class="grid grid-cols-1">
            <div class=" col-span-2 p-6">
                <form wire:submit.prevent="submit">

                    <div class=" overflow-hidden sm:rounded-md">
                            <div class="grid grid-cols-6 gap-6">


                                <div class="col-span-6 ">
                                    <label for="amount" class="block text-sm font-medium text-gray-700">amount</label>
                                    <input type="number" step="any" wire:model.debounce.500ms="amount" id="amount"  class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    @error('amount') <span class="text-red-500">{{ $message }}</span> @enderror

                                </div>

                                <div class="col-span-6">
                                    <label for="currency" class=" block text-sm font-medium text-gray-700">currency</label>
                                    <select   wire:model="currency" id="currency" class=" py-2 mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                        <option disabled>Select Currency</option>
                                        <option value="EUR" selected>Euro</option>
                                        <option value="USD">US Dollar</option>
                                        <option value="CAD">Canadian Dollar</option>
                                        <option value="GBP">Pound Sterling</option>
                                    </select>
                                    @error('currency') <span class="text-red-500">{{ $message }}</span> @enderror

                                </div>



                                <div class="col-span-6">
                                    <label for="notes" class="block text-sm font-medium text-gray-700">notes</label>
                                    <textarea type="text" wire:model.debounce.500ms="notes" id="notes"  class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                                    @error('notes') <span class="text-red-500">{{ $message }}</span> @enderror

                                </div>
                                <div class="col-span-6 bg-gray-50">

                                    <button type="submit" class="w-full justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        Submit
                                    </button>


                            </div>

                    </div>

                </form>

            </div>

        </div>
    </div>

</div>
