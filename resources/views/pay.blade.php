<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="row">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900">
                        <div class="flex flex-col">
                            <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                                    <div class="overflow-hidden">
                                       <b>notes :</b> {{$card['notes']}}
                                        <table class="min-w-full">
                                            <thead class="border-b">
                                            <tr>
                                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                                    #
                                                </th>
                                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                                    amount
                                                </th>
                                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                                    currency
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="bg-white border-b">
                                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{{$card['id']}}</td>

                                                <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {{$card['amount']}}
                                                </td>
                                                <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {{$card['currency']}}
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="smart-button-container">
                            <div style="text-align: center;">
                                <div id="paypal-button-container"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


        <script
            src="https://www.paypal.com/sdk/js?client-id={{$ClientID}}&enable-funding=venmo&disable-funding=sofort,sepa&currency={{$Currency}}"
            data-sdk-integration-source="button-factory"></script>
        <script>
            var token = "{{ csrf_token() }}";

            function initPayPalButton() {
                paypal.Buttons({
                    style: {
                        shape: 'rect',
                        color: 'gold',
                        layout: 'vertical',
                        label: 'paypal',
                    },

                    createOrder: async function (data, actions) {
                        return await fetch("{{route('paypal.create')}}", {
                            method: "post",
                            headers: {
                                'Accept': 'application/json, text/plain, */*',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                id: {{$id}},
                                _token:token,

                                // use the "body" param to optionally pass additional order information
              // like product ids or amount
                            }),
                        })
                            .then((response) => response.json())
                            .then((order) => order.id);
                    },
                    onApprove: function (data, actions) {
                        return fetch(`{{route('paypal.capture')}}`, {
                            method: "post", headers: {
                                'Accept': 'application/json, text/plain, */*',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                payment_orderId: data.orderID,
                                order_id: {{$id}},
                                _token:token,

                            }),
                        })
                            .then((orderData) => {

                                // Or go to another URL:
                                const element = document.getElementById('paypal-button-container');
                                element.innerHTML = '';
                                setTimeout( function(){
                                    element.innerHTML = '<h3>Thank you for your payment!</h3>';
                                }, 1500);                        });
                    },

                    onError: function (err) {
                        const element = document.getElementById('paypal-button-container');
                        element.innerHTML = '';
                        setTimeout( function(){
                            element.innerHTML = '<h3>Thank you for your payment!</h3>' +
                                '<a class="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out" href="{{route('cards.duplicate',$id)}}">duplicate card</a>';
                        }, 1500);                }
                }).render('#paypal-button-container');
            }

            initPayPalButton();

        </script>
</x-app-layout>



