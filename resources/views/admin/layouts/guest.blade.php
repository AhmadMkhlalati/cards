<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset('/admin/img/logo/logo.png')}}" rel="icon">
    <link href="{{asset('/admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/admin/css/ruang-admin.min.css')}}" rel="stylesheet">
    @laravelViewsStyles

</head>
<body class="font-sans antialiased">
<main>

    @yield('content')

</main>
</body>

<script src="{{asset('/admin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('/admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('/admin/js/ruang-admin.min.js')}}"></script>
@laravelViewsScripts
</html>
