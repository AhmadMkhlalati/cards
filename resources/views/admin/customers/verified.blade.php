@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <livewire:customers-verified-table-view userId="{{$user->id}}"/>

    </div>

@endsection
