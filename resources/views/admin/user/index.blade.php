@extends('admin.layouts.app')

@section('content')

    <div class="container ">
        <div class="card w-full">

            <div class="row">

                <div class="col-sm-3 p-10">
                    <img src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                         style="width: 150px;border-radius:50%"
                    />

                </div>
                <div class="col-sm-9 my-6">

                    <div class="card-body">
                        <h3 class="card-title"><b>Name :</b> {{$user->name}}</h3>
                        <p class="card-text"><b>Email :</b> {{$user->email}}</p>
                        <p class="card-text"><b>Created :</b>{{$user->created_at}}</p>

                    </div>
                                        <a class="btn btn-primary my-3" href="{{route('admin.user.paying',$user->id)}}">make payed</a>

                    <div class="row">

                        <div class="col-md-3 flex  rounded-lg">
                            <div
                                class="inline-flex flex-shrink-0 items-center justify-center h-16 w-16 text-green-600 bg-green-100 rounded-full mr-6">
                                $
                            </div>
                            <div>
                                <span class="inline-block text-2xl font-bold">{{$user->USD}}</span>
                                <span class="block text-gray-500"> USD</span>
                            </div>
                        </div>
                        <div class="col-md-3 flex  rounded-lg">
                            <div
                                class="inline-flex flex-shrink-0 items-center justify-center h-16 w-16 text-green-600 bg-green-100 rounded-full mr-6">
                                €
                            </div>
                            <div>
                                <span class="inline-block text-2xl font-bold">{{$user->EUR}}</span>
                                <span class="block text-gray-500">EURO</span>
                            </div>
                        </div>
                        <div class="col-md-3 flex  rounded-lg">
                            <div
                                class="inline-flex flex-shrink-0 items-center justify-center h-16 w-16 text-green-600 bg-green-100 rounded-full mr-6">
                                £
                            </div>
                            <div>
                                <span class="inline-block text-2xl font-bold">{{$user->GBP}}</span>
                                <span class="block text-gray-500">GBP</span>
                            </div>
                        </div>
                        <div class="col-md-3 flex  rounded-lg">
                            <div
                                class="inline-flex flex-shrink-0 items-center justify-center h-16 w-16 text-green-600 bg-green-100 rounded-full mr-6">
                                c$
                            </div>
                            <div>
                                <span class="inline-block text-2xl font-bold">{{$user->CAD}}</span>
                                <span class="block text-gray-500">CAD</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <livewire:user-cards-table-view userId="{{$user->id}}"/>

        </div>

@endsection
